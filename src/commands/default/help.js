import {Command} from '../../framework/command'
import {commandList} from '../../framework/commandLoader'
import {commandLogger, testLogger} from '../../framework/loggers'
import {RichEmbed} from 'discord.js'

function getHelpInfo(commands){
    let info = "";
    let counter = 1;
    for(const command of commands){
        info += `${counter} **${command.details.name}** `+
                `\`>${command.details.example}\` `+
                `*${command.details.description}*\n`
        counter++;
    }
    return info;
}

const Help = new Command({
    name: "help",
    description: "lists information about all the commands",
    example: "help",
    run: (msg) => {
        commandLogger.debug('help - start');

        const title = "Help | Command list";
        const description = getHelpInfo(commandList);
        
        var embed = new RichEmbed();
        embed.setTitle(title);
        embed.setDescription(description);
        embed.setColor('WHITE');
        msg.channel.send("",{embed: embed});

        commandLogger.debug('help - end');
    },
    mock: (testChannel) => {
        testLogger.debug('Mocking.. | >help')
        return testChannel.send('>help');
    },
    assert: (lastMessage) => {
        let titleCheck = false;
        try {
            testLogger.debug('Asserting help command.. [embed title] | target: Help | Command list');
            titleCheck = lastMessage.embeds[0].title === "Help | Command list";
            // assert description (TODO)
        } catch (error) {
            testLogger.error(error);
        }
        
        return titleCheck;
    }
});

export {Help as default};