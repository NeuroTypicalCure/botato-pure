import {Command} from '../../framework/command'
import {commandLogger, testLogger} from '../../framework/loggers'
import {parse} from '../../framework/parse'
import {execute} from '../../framework/music'

const Play = new Command({
    name: "play",
    description: "plays music links",
    example: "play <url>",
    run: async (msg) => {
        try {
            execute(msg);
        } catch (error) {
            commandLogger.error(error);
        }
    },
    mock: (testChannel) => {
        testLogger.debug('mocking.. send: >ping')
        return testChannel.send('>ping');
    },
    assert: (lastMessage) => {
        testLogger.debug('asserting.. target: Pong')
        return lastMessage.content === "Pong";
    }
});

export {Play as default};