import {Command} from '../../framework/command'
import {commandLogger, testLogger} from '../../framework/loggers'
import {parse} from '../../framework/parse'
import {skip} from '../../framework/music'

const Skip = new Command({
    name: "skip",
    description: "skips song",
    example: "skip",
    run: async (msg) => {
        skip(msg);
    },
    mock: (testChannel) => {
        testLogger.debug('mocking.. send: >ping')
        return testChannel.send('>ping');
    },
    assert: (lastMessage) => {
        testLogger.debug('asserting.. target: Pong')
        return lastMessage.content === "Pong";
    }
});

export {Skip as default};