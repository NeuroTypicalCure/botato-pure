import {Command} from '../../framework/command'
import {commandLogger, testLogger, slcLogger} from '../../framework/loggers'

const Log = new Command({
    name: "log",
    description: "logs previous message in a special log on the server",
    example: "log",
    run: async (msg) => {
        commandLogger.debug('log - start');

        const lastTwo = await msg.channel.fetchMessages({limit: 2});

        commandLogger.debug("logging.. - lastMessage: "+ lastTwo.last().content);
        slcLogger.debug(`User: ${lastTwo.last().author}, content: ${lastTwo.last().content}`);
        msg.channel.send("logged");

        commandLogger.debug('log - end');
    },
    mock: async (testChannel) => {
        testLogger.debug('Mocking.. | >log')
        return await testChannel.send('>log');
    },
    assert: (lastMessage) => {
        // TODO: check logfile instead
        testLogger.debug('Asserting.. | target: logged')
        return lastMessage.content === "logged";
    }
});

export {Log as default};