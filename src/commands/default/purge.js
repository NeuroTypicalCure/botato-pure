import {Command} from '../../framework/command'
import {parse} from '../../framework/parse'
import {testLogger,commandLogger} from '../../framework/loggers'
import {Permissions} from 'discord.js'

async function normalPurge(msg,amount){
    try {
        commandLogger.debug('Purge - selected normal purge');
        const deletedMessages = await msg.channel.bulkDelete(amount+1);
        commandLogger.debug(`Purge (normal) - completed | deleted ${deletedMessages.size} messages`);
    } catch (error) {
        commandLogger.error(error);
    }
}

async function sniperPurge(msg,start, amount){
    try {
        commandLogger.debug('Purge - selected sniper purge');
        commandLogger.debug(`Purge (sniper) - from ${start} delete ${amount} messages`);
        
        const before = await msg.channel.fetchMessages({limit:start+1});
        commandLogger.debug(`Purge (sniper) - after message:`);
        commandLogger.debug(before.last().content);

        const msgsToDelete = await msg.channel.fetchMessages({before:before.last().id,limit:amount});
        commandLogger.debug(`Purge (sniper) - deleting ${msgsToDelete.size} messages..`);
        const deletedMessages = await msg.channel.bulkDelete(msgsToDelete);
        commandLogger.debug(`Purge (sniper) - completed | deleted ${deletedMessages.size} messages`);
    } catch (error) {
        commandLogger.error(error);
    }
}

const Purge = new Command({
        name: "purge",
        description: "command to purge messages between two places",
        example: "purge 0 5",
        permission: Permissions.ADMINISTRATOR,
        run: async (msg) => {
            try {
                commandLogger.debug('Purge - start');
                const parsed = parse(msg);
                const firstInt = parseInt(parsed.args[0]);
                const secondInt = parseInt(parsed.args[1]);
    
                commandLogger.debug(`Purge - ${parsed.args.length} arguments`);
                if(parsed.args.length > 1){
                    await sniperPurge(msg, firstInt, secondInt)
                    await msg.delete(); // delete the message containing the call you made to this command
                }else if(parsed.args.length === 1){
                    await normalPurge(msg, firstInt);
                    //await msg.delete(); // delete the message containing the call you made to this command
                }
                commandLogger.debug(`Purge - end`)
            } catch (error) {
                commandLogger.error(error);
            }
        },
        mock: async (testChannel) => {
            testLogger.debug(`Testing.. - Purge | send hello1`);
            await testChannel.send('hello1');
            testLogger.debug(`Testing.. - Purge | send snipe this`);
            await testChannel.send('snipe this');
            testLogger.debug(`Testing.. - Purge | send hello2`);
            await testChannel.send('hello2');
            testLogger.debug(`Testing.. - Purge | send hello3`);
            await testChannel.send('hello3');
            testLogger.debug(`Testing.. - Purge | send >purge 3`);
            const purgeCommand = await testChannel.send('>purge 3')
            return purgeCommand;
        },
        assert: (msg) => {
            testLogger.debug(`Asserting.. - Purge | message.content: ${msg.content} target: hello1`)
            return msg.content === 'hello1';
        } 
})

export {Purge as default};

