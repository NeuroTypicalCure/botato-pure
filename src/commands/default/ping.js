import {Command} from '../../framework/command'
import {commandLogger, testLogger} from '../../framework/loggers'

const Ping = new Command({
    name: "ping",
    description: "Command to test latency",
    example: "ping",
    run: (msg) => {
        commandLogger.debug('Ping - start');
        msg.channel.send("Pong");
        commandLogger.debug('Ping - end')
    },
    mock: (testChannel) => {
        testLogger.debug('mocking.. send: >ping')
        return testChannel.send('>ping');
    },
    assert: (lastMessage) => {
        testLogger.debug('asserting.. target: Pong')
        return lastMessage.content === "Pong";
    }
});

export {Ping as default};