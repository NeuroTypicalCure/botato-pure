const connector = require('../../library/db-connector');

module.exports = {
    details: {
        name: "daily",
        alias: ["harvest","timely"],
        description: "farm you field daily to get potatoes",
        example: "harvest",
        cooldown: {seconds: 5}
    },
    reply: function(gained, silo){
        return `Gained **${gained}** tatoes! \n`+ `Your silo currently has **${silo}** tatoes`
    },
    run: async function(msg){
    
        const modifier = 0;
        const daily = 300;
    
        try{
            let r = await Promise.all([
                    connector.addTatoes(msg.author,daily),
                    connector.findTatoes(msg.author)
            ]);
    
            msg.channel.send(
                this.reply(daily,r[1].get('tatoes'))
            );
        }catch(err){
            console.log(err);
        }
    }
};