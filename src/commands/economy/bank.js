const parser = require('../../library/msg-parse');
const connector = require('../../library/db-connector');

module.exports = {
    details: {
        name: "bank",
        alias: ["silo","vault"],
        description: "show player currency",
        example: "bank",
        cooldown: {seconds: 0}
    },
    reply: function(username, tatoes){
        return `**${username}**\'s silo currently has **${tatoes}** tatoes`
    },
    run:async function(msg){
        
        let target = parser.ifMentionElseAuthor(msg);

        try{
            let r = await connector.findTatoes(target);
            msg.channel.send(
                this.reply(target.username,r.get('tatoes'))
            );
        }catch(err){
            console.log(err);
        }
    }
}