const Discord = require('discord.js');
const Parse = require('../../library/msg-parse');
const dbc = require('../../library/db-connector');
const sorter = require('../../library/sorter');

// sets channel to image type

module.exports = {
    details: {
        name: "channelinfo",
        description: "shows which channels are set",
        example: "channelinfo",
        permission: Discord.Permissions.FLAGS.ADMINISTRATOR
    },
    run: async function(msg){
        let info = await sorter.getChannelInfo();
        msg.channel.send(
            `Sorter channel: ${info.sorterChannel.name}\nImage channel: ${info.imageChannel.name}\nMusic channel: ${info.musicChannel.name}`
        );
    }
}