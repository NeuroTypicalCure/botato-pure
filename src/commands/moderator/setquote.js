const Discord = require('discord.js');
const Parse = require('../../library/msg-parse');
const db = require('../../datasources/local-db/local-db');

module.exports = {
    details: {
        name: "setquote",
        description: "set quote channel",
        example: "setquote #channel",
        permission: Discord.Permissions.FLAGS.ADMINISTRATOR
    },
    run: function(msg){
        const parse = Parse.breakup(msg.content);

        var channel = parse.parts.first;
        const regex = /\<#(\d+)\>/
        const channelId = channel.match(regex)[1];

        db.Guild.update(
            { quoteChannelId: channelId },
            { where: { id: msg.guild.id } }
        )
        .then(result => msg.reply("Quote channel updated to: "+channel) )
        .catch(err => console.log('setquote-guild-update-err:', err) );
    }
}