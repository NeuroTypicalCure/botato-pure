const Discord = require('discord.js');
const Watcher = require('../../library/watcher');

const ReactionRoles = require('../../library/reaction-roles');

module.exports = {
    details: {
        name: "crr",
        description: "create custom reaction role message, ONLY USE DEFAULT TWITTER EMOJI",
        example: "crr :emoji: rolename | :emoji: rolename|:emoji: rolename",
        permission: Discord.Permissions.FLAGS.ADMINISTRATOR
    },
    run: async function(msg){

        // order the message content
        const parse = await ReactionRoles.parse(msg);
        // generate an image from the input, showing all the roles and their representive emoji
        const attachment = await ReactionRoles.generateRRImg(parse);
        // send this message with the image as attachment
        const message = await msg.channel.send("",attachment);
        // add the reactions that members can click
        ReactionRoles.addReactions(message,parse);
        // act whenever a click on one of the emojis happens, then add the specified role to that member
        Watcher.watchReactions(message,parse);

    }
}