const Discord = require('discord.js');
const Parse = require('../../library/msg-parse');
const dbc = require('../../library/db-connector');

module.exports = {
    details: {
        name: "set-channel-type",
        description: "set channel type for autosort: sorter | image | music",
        example: "set-channel-type channeltype #channel",
        permission: Discord.Permissions.FLAGS.ADMINISTRATOR
    },
    run: async function(msg){
        let parse  = Parse.breakup(msg.content);
        let type = parse.parts[0];

        // cache the current channel for this guild
        
        // 

        let channel = await Parse.findChannelById(msg.guild,Parse.parseChannelId(msg.content));
        dbc.setChannelType(channel,type)
        .then(result => msg.reply(`${channel} is set to recieve images from sorter channels`) )
        .catch(err => console.log('setimage-update-err:', err) );
    }
}