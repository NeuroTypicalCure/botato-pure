const Sequelize = require('sequelize');
const sequelize = new Sequelize('sqlite:./datasources/local-db/database.sqlite',{logging: false});

const type = {
    STRING: Sequelize.STRING,
    INT: Sequelize.INTEGER,
    NUMBER: Sequelize.NUMBER,
    DATE: Sequelize.DATE
}

const User = sequelize.define('user', {
    id: {
        type: type.STRING,
        unique: true,
        allowNull: false,
        primaryKey: true
    },
    name: {
        type: type.STRING,
        allowNull: false
    },
    discriminator: {
        type: type.STRING,
        allowNull: false
    },
    // Irl-link
    age: {
        type: type.INT,
        defaultValue:null,
        allowNull:true 
    },
    birthday: {
        type: type.DATE,
        defaultValue: null,
        allowNull: true
    },
    timezone: {
        type: type.STRING,
        defaultValue: null,
        allowNull: true
    },
    // Economy
    tatoes: {
        type: type.INT,
        defaultValue: 0,
        allowNull: false
    },
    lastHarvest: {
        type: type.DATE,
        defaultValue: new Date('1994-04-12T12:00:00'),
        allowNull: false
    },
    // Medals
    stars: {
        type: type.INT,
        defaultValue: 0,
        allowNull: false
    },
    // Waifu
    ownerId: {
        type: type.STRING,
        unique: false,
        defaultValue: null,
        allowNull: true
    },
    ownerName: {
        type: type.STRING,
        defaultValue: null,
        allowNull: true
    },
    ownerDiscriminator:{
        type: type.STRING,
        defaultValue: null,
        allowNull: true
    },
    price: {
        type: type.INT,
        defaultValue: 0,
        allowNull: false
    },
    wifeId:{
        type: type.STRING,
        defaultValue: null,
        allowNull: true
    },
    wifeName: {
        type: type.STRING,
        defaultValue: null,
        allowNull: true
    },
    wifeDiscriminator:{
        type: type.STRING,
        defaultValue: null,
        allowNull: true
    }
  }, {
    // options
});

const Channel = sequelize.define('channel',{
    id: {
        type: type.STRING,
        unique: true,
        allowNull: false,
        primaryKey:true
    },
    guildId: {
        type: type.STRING,
        references: 'guilds',
        referencesKey: 'id'
    },
    name: {
        type: type.STRING,
        allowNull: false
    },
    type: {
        type: type.STRING,
        allowNull: false,
        defaultValue: 'none'
    }
})

const Guild = sequelize.define('guild', {
	id: {
	  type: type.STRING,
	  unique: true,
      allowNull: false,
      primaryKey:true
	},
	name: {
	    type: type.STRING,
	    allowNull: false
    },
    prefix: {
        type: type.STRING,
        allowNull: false,
        defaultValue: '>'
    },
    // Channels
	birthdayChannelId: {
		type: type.STRING,
		defaultValue: null,
		allowNull: true
	},
	confessionChannelId: {
        type: type.STRING,
        defaultValue: null,
		allowNull: true
    },
    quoteChannelId: {
        type: type.STRING,
        defaultValue: null,
		allowNull: true
    },
    storeChannelId: {
        type: type.STRING,
        defaultValue: null,
		allowNull: true
    },
    imgChannelId: {
        type: type.STRING,
        defaultValue: null,
		allowNull: true
    }
  }, {
	// options
});

// db proxy
function DatabaseProxy(db){
    this.db = db;
    this.guilds = null;
    // guilds
    // channels
    // users
    this.addGuild = function(guild){
        // add guild to database
        db.Guild.upsert(
            {
                id: guild.id,
                name: guild.name,
            },
            { returning: false }
        ).catch(err=>{
            console.warn(err);
        })
        // reset cache to allow update
        this.guilds = null;
    }
    this.addChannel = function(channel){
        // add a channel to database
        db.Channel.upsert(
            {
                id: channel.id,
                name:channel.name
            },
            { returning: false }
        ).catch(err=>{
            console.warn(err);
        })
        // reset channels cache for this guild to allow update
        this.guilds.find(g=>g.id===channel.guild.id).channels = null;
    }
    this.addUser = function(member){
        // add a user to database
        User.upsert(
            {
                id: member.id,
                name: member.user.name,
                discriminator: member.user.discriminator
            },
            { returning: false }
        ).catch(err=>{
            console.warn(err);
        })
        // reset users cache for this guild to allow update
        this.guilds.find(g=>g.id === member.guild.id).users = null;
    }
    this.getGuildById = async function(id){
        if(this.guilds  == null){
            // get info from database
            this.guilds = await db.Guild.findAll();
        }
        return this.guilds.find(g=>g.id === id);
    }
    this.getChannelById = async function (id){
        if(this.channels == null){
            // get info from database
            this.channels = await db.Channel.findAll();
        }
        return this.channels.find(c=>c.id===id);
    }
    this.getUserById = async function(id){
        if(this.users == null){
            // get info from database
            this.users = await db.User.findAll();
        }
        return this.users.find(u=>u.id===id);
    }
}

// METHODS
function clientSync(client){
    client.guilds.map(e=>{

        // insert guild into our db
        Guild.upsert(
            {
                id: e.id,
                name: e.name,
                channels: [...e.channels]
            }, // Record to upsert
            { returning: false }// Return upserted record
        ).catch(err=>{
            console.warn(err);
        })

        // insert all channels of that guild
        e.channels.map(f=>{
            let channel = {
                id: f.id,
                name: f.name,
                guildId: e.id
            }
            Channel.upsert(
                channel, // Record to upsert
                { returning: false }// Return upserted record
            ).catch(err=>{
                console.warn(err);
            })
        })

        // insert all members of that guild
        e.members.map(f=>{
            let user = {
                id: f.id,
                name: f.user.username,
                discriminator: f.user.discriminator
            }

            User.upsert(
                user, // Record to upsert
                { returning: false }// Return upserted record
            ).catch(err=>{
                console.warn(err);
            })
        });
    });
}

async function findChannel(channel){
    const result = await Channel.findAll({
        limit: 1,
        where: {
            id: channel.id
        }
    });
    return result;
}

async function findGuild(guild){
    const result = await Guild.findAll({
        limit: 1,
        where: {
            id: guild.id
        }
    });
    return result;
}

async function findQuoteChannel(guild){
    const g = await Guild.findOne({
        where: {
            id: guild.id
        }
    });
    const quoteChannel = await guild.channels.find(e=>e.id === g.quoteChannelId);
    return quoteChannel;
}

// CONFIG
Guild.hasMany(User);
Guild.hasMany(Channel);
sequelize.sync({ alter: true });
//sequelize.sync({force:true})

module.exports = {
    Sequelize: sequelize,
    User: User,
    Channel: Channel,
	Guild: Guild,
	Type: type,
    Op: Sequelize.Op,
    clientSync: clientSync,
    findChannel: findChannel,
    findGuild: findGuild,
    findQuoteChannel: findQuoteChannel
}