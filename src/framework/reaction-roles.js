const Discord = require('discord.js');
const Parse = require('./msg-parse');
const Canvas = require('canvas');
const Twemoji = require('twemoji');

module.exports = function(){

    this.emojiScale = 0.6;
    this.scale = 0.6;
    this.roles = [];

    this.parse = async function(msg){
        var result = [];

        // remove the prefix and command from the message
        var content = Parse.breakup(msg.content).after;
        // split the emojiroles
        content = content.split("|");
    

        for (let i = 0; i < content.length; i++) {
            var el = content[i];

            //split emoji from role
            el = el.trim();
            el = el.split(' ');

            //convert emoji to an image source
            var src = Twemoji.parse(
                el[0]
            );
            // twemoji.parse returns html <img> tag, so this matches the source argument in that html
            src = src.match(/src="(.+)"/)[1];

            // load the image from the emoji src link
            const emojiImg = await Canvas.loadImage(src);

            //get role data from discord
            const request = await msg.guild.roles.find(role => role.name === el[1]);

            this.roles = request;
            // role.color from discord is base 10, so we need to convert to hex
            //push all this into an array
            result.push({emoji: {char:el[0],img: emojiImg,width:Math.round(emojiImg.width*this.emojiScale),height:Math.round(emojiImg.height*this.emojiScale)},role: {name:request.name,color:"#"+request.color.toString(16)}});
        }

        return result;
    }

    this.drawItem = async function(ctx,el,x,y){
         // could use var text = ctx.measureText('Awesome!') and then text.width in the future but for now this works
         const textSize = 50*this.scale;
         ctx.drawImage(el.emoji.img, x, y,el.emoji.width,el.emoji.height);
         // all other fonts look broken except impact
         ctx.font = textSize+"px Arial";
         ctx.fillStyle = el.role.color;
         // tries to keep the role name centered next to the emoji image
         ctx.fillText(el.role.name, x+el.emoji.width+10, y+(el.emoji.height/2)+(textSize/2));
    }

    this.drawItems = async function(canvas,ctx,data,cols,rows,x,y){
        
        // TODO: calculate the size of the role text and modify spacing automatically

        const itemWidth = 300*this.scale;
        const itemHeight = 80*this.scale;

        const spacingWidth = 320*this.scale;
        const spacingHeight = 20*this.scale;

        let a = x;
        let b = y;

        let i = 0;
        for (let j = 0; j < rows; j++) {
            for (let k = 0; k < cols; k++) {
                let el = data[i];
                a = k*(itemWidth+spacingWidth)+x;
                b = j*(itemHeight+spacingHeight)+y;

                this.drawItem(ctx,el,a,b);
                i++;
            }
        }
    }

    this.calcCanvasHeight = function(maxRows){
        var h = 0;
        switch(maxRows){
            case 1: h = 110;
            break;
            case 2: h = 85;
            break;
            case 3: h = 75;
            break;
            case 4: h = 70;
            break;
            case 5: h = 68;
            break;
            case 6: h = 65;
            break;
        }
        return maxRows*h;
    }

    this.cardStyle = function(canvas,ctx){
        // add background, same color as discord background
        ctx.fillStyle = "#36393F";
        ctx.fillRect(0,0,canvas.width,canvas.height);

        // add lighting top and bottom
        ctx.strokeStyle = "grey";
        ctx.beginPath();
        ctx.moveTo(0,0);
        ctx.lineTo(canvas.width, 0);
        ctx.moveTo(0,canvas.height);
        ctx.lineTo(canvas.width,canvas.height);
        ctx.stroke();
    }

    this.generateRRImg = async function(data){
        var parse = data;

        // Send a message if the maximum roles on a post is exceeded
        if(parse.length > 24){
            msg.channel.send("Maximum reaction roles on one post is 24");
        }

        // Calculate the amount of rows that are going to be needed
        const cols = 2;
        const rows = Math.ceil(parse.length/cols);

        // create an canvas context
        var canvas = Canvas.createCanvas(800,400);
        var ctx = canvas.getContext("2d");

        canvas.height = this.calcCanvasHeight(rows);

        this.cardStyle(canvas,ctx);
        this.drawItems(canvas,ctx,parse,cols,rows,90,25);
        //this.addReactions(parse);
        //this.watchReactions();

        const attach = new Discord.Attachment(canvas.toBuffer(), "img.png");
        return attach;
    }

    this.addReactions = async function(msg,data){
        for (let i = 0; i < data.length; i++) {
            const unicode = data[i].emoji.char;
            
            // add initial reactions
            await msg.react(unicode);
        }
    }

    return this;
}();