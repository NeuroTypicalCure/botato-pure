import Config from '../bot.config.json';

const PREFIX_LENGTH = Config.prefix.length;

function parse(msg){
    const content = msg.content.trim();
    const split = content.split(' ');

    const msgPrefix = content.substr(0,PREFIX_LENGTH);
    const msgCommand = split[0].substr(PREFIX_LENGTH);
    const msgOptions = content.substr(content.indexOf(' ')+1);
    const msgArgs = msgOptions.split(' ');

    function hasPrefix(botPrefix){
        if(!botPrefix && msgPrefix) return true;
        else if(msgPrefix === botPrefix) return true;
        else return false;
    }
    function hasCommand(command){
        if(!command && msgCommand) return true;
        else if(msgCommand === command) return true;
        else return false;
    }
    function hasOptions(options){
        if(!options && msgOptions) return true;
        else if(msgOptions === options) return true;
        else return false;
    }

    return {
        content: content,
        isBot: msg.author.bot,
        hasPrefix: hasPrefix,
        hasCommand: hasCommand,
        hasOptions: hasOptions,
        prefix: msgPrefix,
        command: msgCommand,
        options: msgOptions,
        args: msgArgs
    }
}
export {parse};