// when a channel is set as sorted this module will sort each message in there and post it to
// the correct channel instead for example music to #music
// optional a command that will sort to a specific channel
const Discord = require('discord.js');
const dbc = require('./db-connector');
const db = require('../datasources/local-db/local-db');
const parse = require('./msg-parse');

module.exports = {
    // FUTURE TODO: Move these channels states into an object outside of this file
    sorterChannel: null,
    imageChannel: null,
    musicChannel: null,
    getChannelInfo: async function(){
        await this.sync();
        let channelInfo = {
            sorterChannel: {
                name: this.sorterChannel.name || 'not set'
            },
            imageChannel: {
                name: this.imageChannel.name || 'not set'
            },
            musicChannel: {
                name: this.musicChannel.name || 'not set'
            }
        }
        return channelInfo;
    },
    sync: async function(){
        try {
            this.sorterChannel = await dbc.findChannelByType('sorter');
            this.imageChannel = await dbc.findChannelByType('image');
            this.musicChannel = await dbc.findChannelByType('music');
        } catch (error) {
            console.log(error);
        }
    },
    hasSorterChannel: this.sorterChannel !== null,
    hasImageChannel: this.imageChannel !== null,
    hasMusicChannel: this.musicChannel !== null,
    isMsgInSorterChannel: function(msg){
        return this.sorterChannel.id === msg.channel.id;
    },
    createImgEmbed: function(msg){
        let image = parse.parseImage(msg);
        const embed = new Discord.RichEmbed()
        .setColor('#0099ff')
        .setAuthor(msg.author.username,msg.author.avatarURL)
        .setImage(image.url)
        .setTimestamp()
        return embed;
    },
    createYoutubeEmbed: function(msg){
        //TODO: get link information
        const embed = new Discord.RichEmbed()
        .setColor('#0099ff')
        .setTitle(msg.content)
        .setURL(msg.content)
        //.setImage('https://i1.ytimg.com/vi/Iljrx9OO6U4/hqdefault.jpg')
        .setAuthor(msg.author.username,msg.author.avatarURL)
        .setTimestamp()
        return embed;
    },
    redirectImages: async function(msg,channel){
        if(channel && parse.hasImage(msg)){
            //get real image channel from discord, create an embed and send
            try {
                let imc = await parse.findChannelById(msg.guild,channel.id);
                let embed = this.createImgEmbed(msg);
                imc.send(embed);
            } catch (error) {
                console.log(error);
            }
        }
    },
    redirectYoutube: async function(msg,channel){
        // get music channel
        if(channel && parse.hasYoutube(msg)){
            try {
                let mc = await parse.findChannelById(msg.guild,channel.id);
                let embed = this.createYoutubeEmbed(msg);
                mc.send(embed);    
            } catch (error) {
                console.log(error);
            }
        }
    },
    run: async function(msg){
        console.log('insidesorter');
        //exit if there is no image or no youtube
        let hasImgOrYt = parse.hasImage(msg)||parse.hasYoutube(msg);
        if( !hasImgOrYt ) return;
        console.log('hasimgoryt');

        let sorterChannel=null;
        try {
            sorterChannel = await dbc.getTypedChannel(msg.guild,'sorter');
        } catch (err) {
            console.log(err);
        }

        //exit if there is no sorter channel
        if(!sorterChannel) return;
        console.log('hassortchan');
        //exit if message isn't in a sorter channel
        if(!(msg.channel.id === sorterChannel.id)) return;
        console.log('msginsort');
        let imageChannel=null;
        let musicChannel=null;
        try {            
            imageChannel = await dbc.getTypedChannel(msg.guild,'image');
            musicChannel = await dbc.getTypedChannel(msg.guild,'music');
        } catch (err) {
            console.log(err);
        }

        //exit if there is no destination channel
        if(!(imageChannel||musicChannel)) return;
        
        console.log('SortChannel: '+sorterChannel+'\nimageChannel: '+imageChannel);

        // handle images
        this.redirectImages(msg,imageChannel);
        // handle youtube
        this.redirectYoutube(msg,musicChannel);

        console.log('hasImage: '+parse.hasImage(msg)+'\nhasYoutube: '+parse.hasYoutube(msg));
    }
}