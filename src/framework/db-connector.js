const db = require('../datasources/local-db/local-db');

module.exports = function(){

    /*
        This module acts as the interface to the database or apis
    */
    
    this.findTatoes = async function(user){
        let query = await db.User.findOne({
            where: { id: user.id },
            attributes: ['tatoes']
        });
        return query;
    }

    this.addTatoes = function(user, amount){
        let query = db.User.increment(['tatoes'], { by: amount, where: { id: user.id } });
        return query;
    }

    this.setChannelType = async function(channel,type){
        // video / photo / audio / url
        let query = await db.Channel.update(
            {type: type},
            {where: {id:channel.id}}
        );
        return query;
    }

    this.findChannelsByType = async function(type){
        let query = await db.Channel.findAll(
            {where: {type:type}}
        );
        return query;
    }

    this.findChannelByType = async function(type){
        let query = await db.Channel.findOne(
            {where: {type:type}}
        );
        return query;
    }
    this.getTypedChannel = async function(guild,type){
        let query = await db.Channel.findOne(
            {where: {type:type,guildId:guild.id}}
        );
        return query;
    }







    return this;
}();