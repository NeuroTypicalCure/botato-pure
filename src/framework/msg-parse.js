const config = require('../config');

const emoji = {
    menu: [],
    colors: [],
    job: [],
    activity: [],
    mbti: [],
    abc: []
}

// Menu emojis
emoji.menu['0'] = "0️⃣";
emoji.menu['1'] = "1️⃣";
emoji.menu['2'] = "2️⃣";
emoji.menu['3'] = "3️⃣";
emoji.menu['4'] = "4️⃣";
emoji.menu['5'] = "5️⃣";
emoji.menu['6'] = "6️⃣";
emoji.menu['7'] = "7️⃣";
emoji.menu['8'] = "8️⃣";
emoji.menu['9'] = "9️⃣";
emoji.menu['10'] = "🔟";
emoji.menu['pp'] = "⏯";
emoji.menu['stop'] = "⏹";
emoji.menu['left'] = "◀️";
emoji.menu['right'] = "▶️";
emoji.menu['first'] = "⏮";
emoji.menu['end'] = "⏭";
emoji.menu['ff'] = "⏩";
emoji.menu['fb'] = "⏪";

// Color emojis
emoji.colors['red_apple'] = "🍎";
emoji.colors['orange'] = "🍊";
emoji.colors['lemon'] = "🍋";
emoji.colors['pear'] = "🍐";
emoji.colors['grapes'] = "🍇";
emoji.colors['avocado'] = "🥑";
emoji.colors['corn'] = "🌽";
emoji.colors['cookie'] = "🍪";
emoji.colors['peanut'] = "🥜";
emoji.colors['coffee'] = "☕️";
emoji.colors['milk'] = "🥛";
emoji.colors['hibiscus'] = "🌺";
emoji.colors['fleur_de_lis'] = "⚜️";
emoji.colors['negative_squared_cross_mark'] = "❎";
emoji.colors['earth_africa'] = "🌍";
emoji.colors['basketball'] = "🏀";
emoji.colors['skier'] = "⛷️";
emoji.colors['red_circle'] = "🔴";

// job emojis
emoji.job['tongue'] = "👅";
emoji.job['musical_note'] = "🎵";
emoji.job['monkey_face'] = "🐵";
emoji.job['tiger'] = "🐯";
emoji.job['panda_face'] = "🐼";
emoji.job['bee'] = "🐝";
emoji.job['computer'] = "💻";
emoji.job['eyes'] = "👀";

// activity emojis
emoji.activity['writing_hand'] = "✍️";
emoji.activity['comet'] = "☄️";
emoji.activity['movie_camera'] = "🎥";
emoji.activity['video_game'] = "🎮";
emoji.activity['musical_keyboard'] = "🎹";

// mbti emojis
emoji.mbti['cat'] = "🐱";
emoji.mbti['turtle'] = "🐢";
emoji.mbti['koala'] = "🐨";
emoji.mbti['hamster'] = "🐹";
emoji.mbti['tiger'] = "🐯";
emoji.mbti['ox'] = "🐂";
emoji.mbti['mouse'] = "🐭";
emoji.mbti['elephant'] = "🐘";
emoji.mbti['owl'] = "🦉";
emoji.mbti['eagle'] = "🦅";
emoji.mbti['dolphin'] = "🐬";
emoji.mbti['fox'] = "🦊";
emoji.mbti['lion'] = "🦁";
emoji.mbti['cow'] = "🐮";
emoji.mbti['dog'] = "🐶";

// abc emojis
emoji.abc['a'] = "";
emoji.abc['b'] = "";
emoji.abc['c'] = "";
emoji.abc['d'] = "";
emoji.abc['e'] = "";
emoji.abc['f'] = "";
emoji.abc['g'] = "";
emoji.abc['h'] = "";
emoji.abc['i'] = "";
emoji.abc['j'] = "";
emoji.abc['k'] = "";
emoji.abc['l'] = "";
emoji.abc['m'] = "";
emoji.abc['n'] = "";
emoji.abc['o'] = "";
emoji.abc['p'] = "";
emoji.abc['q'] = "";
emoji.abc['r'] = "";
emoji.abc['s'] = "";
emoji.abc['t'] = "";
emoji.abc['u'] = "";
emoji.abc['v'] = "";
emoji.abc['w'] = "";
emoji.abc['x'] = "";
emoji.abc['y'] = "";
emoji.abc['z'] = "";

module.exports = function(){
    this.result = {
        prefix: '',
        command: '',
        after: '',
        parts: []
    }

    this.breakup = function (content){
        var before = '';

        if(content.indexOf(' ')===-1){
            before = content;
        }else{
            before = content.substr(0,content.indexOf(' '));
            this.result.after = content.substr(content.indexOf(' ')+1);
            this.result.parts = this.result.after.split(" ");
        }

        this.result.prefix = before.substr(0,config.prefix.length);
        this.result.command = before.substr(config.prefix.length);

        console.log('this.result.prefix:', this.result.prefix)
        console.log('command:', this.result.command)

        return this.result;
    }

    // check if message has image
    this.hasImage = function(msg){
        if(msg.attachments.size < 1){
            return false;
        }else{
            let filename = msg.attachments.first().filename;
            let isPNG = filename.indexOf('.png') !== -1;
            let isJPEG = filename.indexOf('.jpeg') !== -1;
            let isJPG = filename.indexOf('.jpg') !== -1;
            let isSVG = filename.indexOf('.svg') !== -1;
            if(isPNG||isJPEG||isJPG||isSVG) return true;
        }
    }

    this.parseImage = function(msg){
        if(this.hasImage(msg)){
            const attachment = msg.attachments.first();
            return attachment;
        }else{
            return null;
        }
    }

    this.hasYoutube = function(msg){
        if(msg.content.length < 1){
            return false;
        }else{
            let isYoutube = msg.content.indexOf('https://www.youtube.com/watch?v=') !== -1;
            if(isYoutube){
                return true;
            }
        }
    }

    this.hasUserMentions = function (msg){
        if(msg.mentions.users.first()){
            return true;
        }else{
            return false;
        }
    }
    this.hasRoleMentions = function (msg){
        if(msg.mentions.roles.first()){
            return true;
        }else{
            return false;
        }
    }

    this.ifMentionElseAuthor = function(msg){
        if(this.hasUserMentions(msg)){
            return msg.mentions.users.first();
        }else{
            return msg.author;
        }
    }

    this.appifyURL = function (url){
        const at = url.indexOf('.com/');
        const result = "https://ptb.discordapp.com"+url.substr(at+4);
        return result;
    }

    this.parseChannelId = function (content){
        const id = content.replace(/\D/g,'');
        return id;
    }

    this.findChannelById = async function (guild,id){
        const result = await guild.channels.find(e=>e.id === id);
        return result;
    }

    this.roleReactions = function (content){
        content = content.replace(/\s/g,'');
        result = content.split(':');
        var blocker = true;
        var ers = [];
        for(var i=0;i<result.length;i++){
            var first = '';
        var second = '';
            if(blocker){
                first = result[i];
                blocker = false;
            }else{
                second = result[i];
                ers.push({role:first,emoji:second});
                blocker = true;
            }
        }
        return ers;
    }

    this.findEmojiByName = async function(client,name){
        name = name.replace(/:/g,'');
        console.log('emoji name:', name)
        const result = await client.emojis.find(emoji => emoji.name === name);
        console.log('emoji result:', result)
        return result;
    }

    return this;
}()
