const fs = require('fs');
const path = require('path');
const MsgParse = require('./msg-parse');
const Discord = require('discord.js');
const moment = require('moment');

let isWin = process.platform === "win32";

module.exports = function(){

    /*
        This is the command capsule .. module
        Imports all the commands from inside the /commands/ dir
        When a discord user references one of them it calls the respective run method
        Scroll down for more details
    */

    this.dirs = []
    this.modules = [];
    this.cmmdDirName = 'commands';
    this.waitingLine = [];

    this.importDir = function(dir){
        // change directory separator based on operating system
        let slash = '/';
        if(isWin){
            slash = '\\';
        }

        this.dirs.push(dir);
        dir = path.resolve()+slash+cmmdDirName+slash+dir;
        const files = fs.readdirSync(dir);
        console.log("Get files from: "+dir);
        files.forEach(file => {
            const fp = path.parse(file);
            const name = fp.name;
            console.log("File name: "+name);
            this.modules.push(require(`${dir}${slash}${name}${fp.ext}`));
        });
        return this;
    }

    this.checkWaitingLine = function(obj){
        for(var i=0; i<this.waitingLine.length;i++){

            if(this.waitingLine[i].command === obj.command && this.waitingLine[i].id === obj.id){
                var targetTime = this.waitingLine[i].since.add(obj.cooldown);
                if(moment().isSameOrAfter(targetTime)){
                    this.waitingLine.splice(i,1);
                    return false;
                }else{
                    return true;
                }
            }
            
        }
    }

    this.checkAlias = function(array, command){
        if(array !== undefined){
            for(let i=0;i<array.length;i++){
                if(command === array[i]){
                    return true;
                }
            }
        }
        return false;
    }

    this.run = function(msg){
        console.log('cmmd loader run');
        /* 
            __This function contains the shared logic preceding every command__
            1. checks the input command name against all the commands and aliases that are available
            2. checks if the user has permission to run this command
            3. checks if the user currently has a cooldown from running the command before

            In addition, 
            This function sets cooldowns should they be specified in the details of the module,
            the way it does this is by creating a waiting line array which the users get pushed into.
            This is the array that gets checked next time to enforce the cooldown.

            It also logs a developer warning if no permissions are set in the details of a module
        */

        const result = MsgParse.breakup(msg.content);
        console.log('msg.content:', msg.content);
        console.log('result.parts.body:', result.parts.body);
        console.log('result.parts.first:', result.parts.first);

        for (var i = 0; i < this.modules.length; i++) {
            const command = this.modules[i];

            const isAlias = this.checkAlias(command.details.alias,result.command);
            if(command.details.name === result.command || isAlias) {
                const permission = command.details.permission || Discord.Permissions.DEFAULT;
                const cooldown = command.details.cooldown || 0;
                const validPermission = msg.member.hasPermission(permission);

                // this is just a developer warning
                if(permission === Discord.Permissions.DEFAULT) console.warn("No permission set for command: "+command.details.name);
                
                if(!validPermission){
                    msg.reply("You don\'t have the required permissions to use this command.");
                }else{
                    // if command has cooldown set
                    if(cooldown !== 0){
                        // is user in waiting line?
                        var waiting = checkWaitingLine({command: command.details.name, id: msg.author.id,cooldown: cooldown});
                        if(waiting === true){
                            msg.channel.send("you\'re still under cooldown");
                        }else{
                            // run command
                            command.run(msg,result);
                            // put user in waiting line
                            this.waitingLine.push({command: command.details.name,id: msg.author.id, cooldown: cooldown, since: moment()})
                        }
                    }else{
                        command.run(msg,result);
                    }
                }
            }
        }
    }
    return this;
}()

