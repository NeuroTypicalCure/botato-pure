import log4js from 'log4js'

log4js.configure({
    appenders: {
      out: { type: 'stdout' },
      app: { type: 'file', filename: './logs/application.log' },
      // server log command check commands/log.js
      slc: { type: 'file', filename: './logs/slc.log' }
    },
    categories: {
        default: { appenders: ['out', 'app'], level: 'error' },
        testing: { appenders: ['out', 'app'], level: 'debug' },
        command: { appenders: ['out', 'app'], level: 'debug' },
        parsing: { appenders: ['out', 'app'], level: 'debug' },
        slcing:  { appenders: ['slc'], level: 'debug' }
        
    }
});

const commandLogger = log4js.getLogger('command');
const testLogger = log4js.getLogger('testing');
const slcLogger = log4js.getLogger('slcing');

export {commandLogger, testLogger, slcLogger}