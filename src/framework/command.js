import {commandList} from './commandLoader'
import {testChannel} from './channels.js'
import {END_TESTING} from './global'
import {commandLogger,testLogger} from './loggers'
import {parse} from './parse'
import {Permissions} from 'discord.js'

let testCounter = 0;

class Command {
    constructor({name,description,example,run,mock,assert,permission}){
        this.details = {
            name: name,
            description: description,
            example: example, 
            permission: permission || Permissions.DEFAULT
        };
        this.run = async (msg) => {
            const member = msg.member;
            if(member.hasPermission(this.details.permission)){
                await run(msg);
            }else{
                commandLogger.debug(`${member.id} does not have permissions to run command: ${this.details.name}`);
            }
        };
        this.test = {
            counter: 0,
            start: async () => {
                try {
                    await testChannel.send('Testing has started..');
                    testLogger.debug("Testing has started..");
                    await this.test.mock();
                } catch (error) {
                    console.error(error);
                }
            },
            mock: async () => {
                testLogger.debug(`Running.. - test ${testCounter+1} of ${commandList.length} | ${this.details.name}`);
                mock(testChannel);
            },
            collect: async () => {
                try {
                    testLogger.debug(`Fetching.. | last message in channel`)
                    const messages = await testChannel.fetchMessages({limit: 1});
                    const lastMessage = messages.first();
                    testLogger.debug(`Fetch complete | message: ${lastMessage.content}`);
                    return lastMessage;
                } catch (error) {
                    console.error(error);
                }
            },
            assert: async () => {
                const collected = await this.test.collect()
                const testResult = assert(collected);
                if(testResult){
                    this.test.successMessage();
                }else{
                    this.test.failMessage();
                }
            },
            next: () => {
                if(testCounter+1 < commandList.length){
                    testLogger.debug('Next test.. '+(testCounter+1)+'/'+commandList.length);
                    testCounter++;
                    commandList[testCounter].test.start();
                }else{
                    testChannel.send('All tests completed!');
                    testLogger.debug('All tests completed!');
                    END_TESTING();
                }
            },
            successMessage: () => {
                testChannel.send('Command: '+ this.details.name + ' | Test successful :)');
                testLogger.debug(`Test successful - test ${testCounter+1} of ${commandList.length} | ${this.details.name}`)
            },
            failMessage: () => {
                testChannel.send('Command: '+ this.details.name + ' | Test failed :(');
                testLogger.debug(`Test failed - test ${testCounter+1} of ${commandList.length} | ${this.details.name}`)
            }
        };
    }
}

export {Command}