import ytdl from 'ytdl-core';
import {parse} from '../framework/parse'
import yts from 'yt-search'

let queue = null;

async function execute(message) {
    const searchTerm = parse(message).options;
    const voiceChannel = await message.member.voiceChannel;
    if (!voiceChannel) return message.channel.send('Ge moet in een voicechannel zitten absjaar');
        const permissions = voiceChannel.permissionsFor(message.client.user);
    if (!permissions.has('CONNECT') || !permissions.has('SPEAK')) {
        return message.channel.send('Kheb geen permissies prutser');
    }

    console.log(searchTerm);
    const result = await yts(searchTerm);
    const video = result.videos[0];
    const song = {
        title: video.title,
        url: video.url,
    };

    if (!queue) {
        queue = {};
        queue.voiceChannel = voiceChannel;
        queue.songs = [];
        // Pushing the song to our songs array
        queue.songs.push(song);
        
        try {
            // Here we try to join the voicechat and save our connection into our object.
            var connection = await voiceChannel.join();
            queue.connection = connection;
            // Calling the play function to start a song
            play(queue.songs[0]);
        } catch (err) {
            // Printing the error message if the bot fails to join the voicechat
            console.log(err);
            queue = null;
            return message.channel.send(err);
        }
    }else {
        queue.songs.push(song);
        console.log(queue.songs);
        return message.channel.send(`${song.title} is toegevoegd`);
    }
}

function play(song) {
    if (!song) {
     queue.voiceChannel.leave();
     queue = null;
     return;
    }

    const dispatcher = queue.connection.playStream(ytdl(song.url))
    .on('end', () => {
     console.log('Music ended!');
     // Deletes the finished song from the queue
     queue.songs.shift();
     // Calls the play function again with the next song
     play(queue.songs[0]);
   })
    .on('error', error => {
     console.error(error);
    });   
}

function skip(message) {
    if (!message.member.voiceChannel) return message.channel.send('You have to be in a voice channel to stop the music!');
    if (!queue) return message.channel.send('There is no song that I could skip!');
    queue.connection.dispatcher.end();
}

function stop(message) {
    if (!message.member.voiceChannel) return message.channel.send('You have to be in a voice channel to stop the music!');
    queue.songs = [];
    queue.connection.dispatcher.end();
}

export {queue,execute,skip,stop}