import fs from 'fs'
import path from 'path'

let loadFile = (folderPath, file) => {
    const parsedFile = path.parse(file);
    const filePath = path.join(folderPath,parsedFile.name+parsedFile.ext);
    const loadedFile = require(filePath).default;
    //Logger.debug(`Command file ${parsedFile.name+parsedFile.ext} loaded`);
    return loadedFile;
}

let loadCommands = (mainDirectory, categories) => {
    const commands = [];
    for(const category of categories){
        console.log('ppp'+path.resolve("build"));
        const categoryPath = path.join(path.resolve("build"), mainDirectory, category);
        const files = fs.readdirSync(categoryPath);
        for(const file of files){
            commands.push(loadFile(categoryPath, file));
        }
    }
    return commands;
};

const commandList = loadCommands("commands", ["default"]);

export {loadCommands, commandList}