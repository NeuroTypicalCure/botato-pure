import client from './client'

let testChannel = null;
client.on('ready', () => {
    testChannel = client.channels.find('name', 'testing');
})

export {testChannel}