const db = require('../datasources/local-db/local-db');
const Discord = require('discord.js');
const msgParse = require('../library/msg-parse');

module.exports = function(){

    this.watchMsgIds = [];
    this.reactionRoles = '';

    this.reactionQuotes = async function (msg){
        const filter = (reaction, user) => {
            return ['🥔'].includes(reaction.emoji.name) && msg.author.bot !== true;
        };
        const DAY = 1000*60*60*24;
        await msg.awaitReactions(filter, {max:1,time:DAY,errors:['time']})
        .then(async (collected) =>{

            const quoteChannel = await db.findQuoteChannel(msg.guild);

            const reaction = collected.first();
            if (reaction.emoji.name === '🥔') {
                // TODO make EMOJIS WORK IN MSG CONTENT
                var embed = new Discord.RichEmbed();
                embed.setTitle(`${msg.author.username}#${msg.author.discriminator}`);
                embed.setURL(msgParse.appifyURL(msg.url));
                embed.setDescription(msg.content);
                embed.setFooter(`🥔${reaction.users.size}`);
                embed.setTimestamp(msg.createdTimestamp);
                embed.setColor('GREEN');
                quoteChannel.send('', {
                    embed: embed
                })
                .then(console.log)
                .catch(console.error);
            }
        })
        .catch(console.error);
    }

    this.prompt = async function (msg,question){
        // TODO repeat until given the right response
        msg.channel.send(question);
        const result = await msg.channel.awaitMessages(m => m.author.id === msg.author.id, { max: 1, time: 10000 });
        return result.first();
    }

    this.watchReactions = function (msg,data){
        this.watchMsgIds.push(msg.id);
        this.reactionRoles = data;
    }

    this.reactionRoleAdd = function (reaction,user){
        this.watchMsgIds.forEach(m =>{
            if(reaction.message.id === m){
                const invalid = !user&&user.bot&&!reaction.message.channel.guild;
                if(!invalid){
                    this.reactionRoles.map(e=>{
                        if(reaction.emoji.name == e.emoji.char){
                            let role = reaction.message.guild.roles.find(r=> r.name == e.role.name);
                            reaction.message.guild.member(user).addRole(role).catch(console.error);
                        }
                    });
                }
            }
        });
    }

    this.reactionRoleRemove = function (reaction,user){
        this.watchMsgIds.forEach(m =>{
            if(reaction.message.id === m){
                const invalid = !user&&user.bot&&!reaction.message.channel.guild;
                if(!invalid){
                    this.reactionRoles.map(e=>{
                        if(reaction.emoji.name == e.emoji.char){
                            let role = reaction.message.guild.roles.find(r => r.name == e.role.name);   
                            reaction.message.guild.member(user).removeRole(role).catch(console.error);
                        }
                    });
                }
            }
        });
    }

    return this;
}();