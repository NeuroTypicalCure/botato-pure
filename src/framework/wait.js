const moment = require('moment');

module.exports = function(){

    this.waitingLine = [];

    this.checkWaitingLine = function(o){
        if(this.waitingLine !== undefined){
            for(var i=0; i<this.waitingLine.length;i++){

                var commandsMatch = this.waitingLine[i].command === o.command;
                var idsMatch = this.waitingLine[i].id === o.id;
    
                if(commandsMatch && idsMatch){
                    var targetTime = this.waitingLine[i].since.add(o.cooldown);
                    var cooldownEnded = moment().isSameOrAfter(targetTime)
    
                    if(cooldownEnded){
                        this.waitingLine.splice(i,1);
                        return {isPermitted: true};
                    }else{
                        return {isPermitted: false, timeLeft: moment.duration(targetTime.diff(moment())).humanize()};
                    }
                }
                
            }
        }
    }

    this.add = function(o){
        this.waitingLine.push(o);
    }

    return this;
}();