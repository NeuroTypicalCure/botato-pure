import Config from './bot.config.json'
import {parse} from './framework/parse'
import {commandList} from './framework/commandLoader'
import client from './framework/client'
import {TESTING} from './framework/global'

client.on('ready', async () => {
  console.log(`Logged in as ${client.user.tag}!`);
  
  if(TESTING){
        const firstCommand = commandList[0];
        await firstCommand.test.start();
  }
  
})
.on('message',  async (msg) => {
    // Filter messages
    if(!TESTING){
        if(parse(msg).isbot) return;
    }
    if(!parse(msg).hasPrefix(Config.prefix)) return;

    // Match commands
    for(const command of commandList){
        if(command.details.name === parse(msg).command) {
            await command.run(msg);
            if(TESTING){
                await command.test.assert();
                command.test.next();
            }
        }
    }
    
})
.on("messageReactionAdd",(reaction,user)=>{
})
.on("messageReactionRemove",(reaction,user)=>{
})
.on('disconnect', () => { console.warn('Disconnected!'); })
.on('reconnecting', () => { console.warn('Reconnecting...'); })
.on('error', console.error)
.on('warn', console.warn)
.on('debug', console.log);

client.login(Config.token);