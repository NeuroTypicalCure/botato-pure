Framework and bot built figuratively from scratch with inspiration from multiple other sources.

Focus lies on decoupling while still keeping the command module concise.
You want commands to be easy to plug in and edit.

Below are the different files who make up the framework.

- **commandLoader** : Imports the commands from a specified folder (default: "commands") and a specified category subfolder. This loader gets called in the commandList file.

- **commandList** : Easy to import list of all commands.

- **client** : singleton containing the discord client object.

- **channels** : will hold references to frequently used default channels fetched by name.

- **command** : object which works as a dynamic interface for each command instance.

- **msg-utils** : contains parse function which is self explanatory.

- **global** : contains global flags (/ environment variables)

If you want to try and run this for yourself you're going to need to create a config file.
There's an examples file in the examples folder.

Features:

- Custom prefix : set a custom prefix in config, can be of any length.
- Self testing : `npm run test` searches for a channel named "testing" in your server and then tests all it's commands in there.

Incoming features: 

- Custom reaction roles: Unique take on how they're created (overengineered).
- Economy system: Something about farming potatoes.
- Reaction menu: because i'm going to need it for future features like the help command.
- Message sorting system: Ever wanted to route images, music or videos to their respective channel?

I'm always learning so you're always welcome to point me in the right direction.

Adding a command:

This is your boilerplate for each command,

```javascript
import {Command} from '../../framework/command'
import {parse} from '../../framework/msg-utils'
import {commandLogger, testLogger} from '../../framework/loggers'

const Example = new Command({
    name: "example",
    description: "a command as an example",
    example: "example argument1 argument2",
    run: (msg) => {
        commandLogger.debug('log messages in command category (shows up when testing)');
        // what you want the command to do
        msg.channel.send("example response");
    },
    mock: (testChannel) => {
        testLogger.debug('Sending Mock Command.. | >example')

        // how you trigger the command when testing (like if you were using it)
        return testChannel.send('>example'); // don't forget prefix
    },
    assert: (lastMessage) => {
        testLogger.debug('Asserting example command.. | example response')

        // what the last message should be if your command worked perfectly
        return lastMessage.content === "example response";
    }
});

export {Example as default};
```
How to run: 

`npm run build`
`npm run start`

Test:

`npm run test`